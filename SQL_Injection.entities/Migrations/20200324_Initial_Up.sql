CREATE TABLE TbBuku
(
 KodeBuku char(3) primary key,
 JudulBuku Varchar(20),
 NamaPengarang Varchar(20)
)

CREATE TABLE TbBenda
(
 KodeBenda char(3) primary key,
 JudulBenda Varchar(20),
 NamaBenda Varchar(20)
)


CREATE TABLE TbBarang
(
 KodeBarang char(3) primary key,
 JudulBarang Varchar(20),
 NamaBarang Varchar(20)
)