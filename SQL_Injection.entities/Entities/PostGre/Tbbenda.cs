﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQL_Injection.entities.Entities.PostGre
{
    [Table("tbbenda")]
    public partial class Tbbenda
    {
        [Key]
        [Column("kodebenda")]
        [StringLength(3)]
        public string Kodebenda { get; set; }
        [Column("judulbenda")]
        [StringLength(20)]
        public string Judulbenda { get; set; }
        [Column("namabenda")]
        [StringLength(20)]
        public string Namabenda { get; set; }
    }
}
