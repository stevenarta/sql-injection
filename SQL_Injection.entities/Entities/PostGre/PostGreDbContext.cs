﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SQL_Injection.entities.Entities.PostGre
{
    public partial class PostGreDbContext : DbContext
    {
        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Tbbarang> Tbbarang { get; set; }
        public virtual DbSet<Tbbenda> Tbbenda { get; set; }
        public virtual DbSet<Tbbuku> Tbbuku { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tbbarang>(entity =>
            {
                entity.HasKey(e => e.Kodebarang)
                    .HasName("tbbarang_pkey");

                entity.Property(e => e.Kodebarang).IsFixedLength();
            });

            modelBuilder.Entity<Tbbenda>(entity =>
            {
                entity.HasKey(e => e.Kodebenda)
                    .HasName("tbbenda_pkey");

                entity.Property(e => e.Kodebenda).IsFixedLength();
            });

            modelBuilder.Entity<Tbbuku>(entity =>
            {
                entity.HasKey(e => e.Kodebuku)
                    .HasName("tbbuku_pkey");

                entity.Property(e => e.Kodebuku).IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
