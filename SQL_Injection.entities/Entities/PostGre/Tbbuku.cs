﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SQL_Injection.entities.Entities.PostGre
{
    [Table("tbbuku")]
    public partial class Tbbuku
    {
        [Key]
        [Column("kodebuku")]
        [StringLength(3)]
        public string Kodebuku { get; set; }
        [Column("judulbuku")]
        [StringLength(255)]
        public string Judulbuku { get; set; }
        [Column("namapengarang")]
        [StringLength(255)]
        public string Namapengarang { get; set; }
    }
}
